package ex_10;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Calc {
	
	private static final String FNAME = "Item2d.bin";
	private Item2d result;
	
	public Calc() {
		result = new Item2d();
	}
	
	public void setResult(Item2d result) {
		this.result = result;
	}

	public Item2d getResult() {
		return result;
	}

	private int calcOctal(int value) {
		return Integer.toOctalString(value).length();
	}

	private int calcHex(int value) {
	    return Integer.toHexString(value).length();
	    }
	
	public Item2d init(int val) {
	    result.setX(val);
	    result.setY(calcOctal(val));
	    result.setZ(calcHex(val));
	    return result;
	    }
	
	
	public void show() {
		System.out.println(result);
	}
	
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new
	FileOutputStream(FNAME));
		os.writeObject(result);
		os.flush();
		os.close();
	}
	
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		result = (Item2d)is.readObject();
		is.close();
	}
}

