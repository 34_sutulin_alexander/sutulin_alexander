package ex_10;

import java.io.Serializable;

public class Item2d implements Serializable {
	// transient
	private int x;
	private int y;
	private int z;
	private static final long serialVersionUID = 1L;

	public Item2d() {
		x = 0;
		y = 0;
		z = 0;
	}

	public Item2d(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int setX(int x) {
		return this.x = x;
	}

	public int getX() {
		return x;
	}

	public int setY(int y) {
		return this.y = y;
	}

	public int getY() {
		return y;
	}
	public int setZ(int z) {
		return this.z = z;
	}

	public int getZ() {
		return z;
	}


	public Item2d setXYZ(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}
	@Override
	public String toString() {
		return "x = " + x + ", y = " + y + ", z = " + z; 
	}
}